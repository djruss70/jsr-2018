document.getElementById("version").innerHTML = "Note: Using 2018 Settings!";

var ATAR_LETTER_NAME = 'AtarEstimateLetters2018.pdf'

// FIXME: update for 2017.  why are there two codes for IND/ISL?
var PRAC_COURSE = /CSL$|DAN$|DRA$|IND$|ISL$|ITL$|MDT$|MPA$|MUS$|PES$|VAR$/;
var PRAC_EXAM_COURSE = /CSL$|DAN$|DRA$|IND$|ISL$|ITL$|MUS$/

// ref: TISC guide
// FIXME: will MAM bonus still count if MAA is included in aggregate
var ILLEGAL_COURSE_COMBOS = [
//	["HBY", "BLY"], // allowed as of 2016
	["ENG", "LIT"],
//	["MAA", "MAM"], // disallowed for 2018 leavers
//	["MAA", "MAS"]  // disallowed for 2018 leavers
];

var ATAR_PREFIX = /^AT|^AE/;

var MUSIC_CODE_RE = /ATMUJ/;
var MUSIC_CODE = "ATMUS";

// ref: email cwallis 27 Apr 2017
var PRAC_COURSE_WEIGHT = {
    CSL: 0.3,
    DAN: 0.5,
    DRA: 0.5,
    IND: 0.4, // FIXME
    ISL: 0.4,
    ITL: 0.3,
    MDT: 0.5,
    MPA: 0.5,
    MUS: 0.5,
    PES: 0.3,
    VAR: 0.5
};


// FIXME: check for full list of LOTE courses
var LOTE_BONUS_COURSE = /ISL$|CSL$|IND$|ARA$/;
var MATH_BONUS_COURSE = /MAM$|MAS$/;

var LOTE_BONUS = 0.1;
var MATH_BONUS = 0.1;


// map for 201x tisc_reg.xlsx
var TISC_COLUMN_MAP = {
	surname : 	    {name: "Surname - Given Names"},
	firstName :  	{name: "Surname - Given Names"},
	studentNum :  	{name: "Appnum"},
	courseCode :  	{name: "Course"},
	schoolMark :  	{name: "Sch Assess"},
	schoolMarkPrac: {name: "Sch Assess Prac"},
	examMark :  	{name: "Raw Exam"},
	examMarkPrac : 	{name: "Raw Exam Prac"},
	modMark :  	    {name: "Mod/Std Sch Assess"},
	modMarkPrac :  	{name: "Mod/Std Std Assess Prac"}, // note typo!
	combMark :  	{name: "Comb Mark"},
	scaledMark :  	{name: "Final Scaled"},
	tiscAtar :  	{name: "ATAR"}
};


// map for Seqta Academic summary (Spreadsheet)
var SEQTA_COLUMN_MAP = {
	surname : 	    {name: "Surname"},
	firstName :  	{name: "First name"},
	studentCode :  	{name: "Student Code"},
	rollGroup :  	{name: "Roll"},
	studentNum :  	{name: "Government ID"},
	courseCode :  	{name: "Subject Code"},
	courseName :  	{name: "Subject"},
	schoolMark :  	{name: "Mod Score"},
	examMark :  	{name: "Field 4"},      // Written Exam
    writtenOverallMark : {name: "Field 5"}, // override Mod Score for PRAC_COURSE only
	schoolMarkPrac: {name: "Field 6"},      // Practical Overall, also used for emamMarkPrac for subjects that don't do school based prac exam
	examMarkPrac :  {name: "Field 7"},      // PRAC_EXAM_COURSE only
};



// enter data for students studying external courses here
var EXTERNAL_COURSES = {
	"123456" : {
	    surname : "",
	    firstName : "",
	    studentCode : "",
        courseCode : "ARA",
        schoolMark : 99.9,
        schoolMarkPrac : 99.9,
        //overallMark : 99.9,
        examMark : 99.9,
        examMarkPrac : 99.9
	}
};

// // scaling overrides
// var SCALING_OVERRIDES = {
//     CSL: { theoryMod   : { m: 1.0, b: 0.0 },
//            pracMod     : { m: 1.0, b: 0.0 },
//            combScaling : { m: 1.0, b: 0.0 },
//            finalScaling: { m: 1.0, b: 0.0 }
//         }
// };

// 2017 TEA/ATAR summary table
// https://www.tisc.edu.au/static/guide/atar-about.tisc
//ATAR	Minimum TEA for ATAR
ATAR_TO_TEA_MAP = [
    [30.05, 132.4],
    [40.00, 157.0],
    [50.00, 178.0],
    [55.00, 187.6],
    [60.05, 197.5],
    [61.00, 199.4],
    [62.00, 201.3],
    [63.00, 203.2],
    [64.05, 205.2],
    [65.00, 207.2],
    [66.00, 209.3],
    [67.00, 211.3],
    [68.00, 213.5],
    [69.00, 215.8],
    [70.00, 218.0],
    [71.00, 220.1],
    [72.00, 222.4],
    [73.00, 224.9],
    [74.00, 227.4],
    [75.00, 230.0],
    [76.00, 232.4],
    [77.00, 234.9],
    [78.00, 237.4],
    [79.00, 240.0],
    [80.00, 242.5],
    [81.00, 245.4],
    [82.00, 248.2],
    [83.00, 251.5],
    [84.00, 254.9],
    [85.00, 258.2],
    [86.00, 263.8],
    [87.00, 264.9],
    [88.00, 269.4],
    [89.00, 273.4],
    [90.00, 277.7],
    [91.00, 282.0],
    [92.00, 286.8],
    [93.00, 292.3],
    [94.00, 298.3],
    [95.00, 304.8],
    [96.00, 312.1],
    [97.00, 320.9],
    [98.00, 332.0],
    [98.50, 339.9],
    [99.00, 350.4],
    [99.50, 366.5],
    [99.70, 377.7],
    [99.90, 394.0],
    [99.95, 404.2],
];



//  reversing array elements
TEA_TO_ATAR_MAP = ATAR_TO_TEA_MAP.map(function (a) { return [a[1], a[0]] } );



function mapTeaToAtar (tea) {

    var estAtar = 0;
    // find closest points in table
    for (var point in TEA_TO_ATAR_MAP) {
        if (tea < TEA_TO_ATAR_MAP[point][0]) {
            break;
        }
    }

    // estimate with linear interpolation
    // return 0 for tea below range

    if (point > 0) {
        var loTea = TEA_TO_ATAR_MAP[point-1][0];
        var loAtar = TEA_TO_ATAR_MAP[point-1][1];
        var hiTea = TEA_TO_ATAR_MAP[point][0];
        var hiAtar = TEA_TO_ATAR_MAP[point][1];

        estAtar = linInterp(tea, {'x': loTea, 'y':loAtar}, {'x':hiTea, 'y': hiAtar});
    }

    // truncate to 0.05
    return Math.floor(estAtar * 20) / 20;
}


function mapAtarToTea (atar) {

    // find closest points in table
    for (var point in TEA_TO_ATAR_MAP) {
        if (atar < TEA_TO_ATAR_MAP[point][1]) {
            break;
        }
    }

    // estimate with linear interpolation
    // return 0 for atar below range
    var estTea = 0;

    if (point > 0) {
        var loTea = TEA_TO_ATAR_MAP[point-1][0];
        var loAtar = TEA_TO_ATAR_MAP[point-1][1];
        var hiTea = TEA_TO_ATAR_MAP[point][0];
        var hiAtar = TEA_TO_ATAR_MAP[point][1];

        estTea = linInterp(atar, {'x': loAtar, 'y':loTea}, {'x':hiAtar, 'y': hiTea});
    }

    return round(estTea, 1);
}




//http://www.studyat.uwa.edu.au/undergraduate/entry-pathways/broadway
var BROADWAY_RULES = [
    // TIER A
    {max: 75, bonus: 0, cap:75},
    {max: 80, bonus: 5, cap:80},
    {max: 85, bonus: 5, cap:89},
    {max: 90, bonus: 4, cap:93},
    {max: 95, bonus: 3, cap:96},
    {max: 98, bonus: 2, cap:98},
    {max:100, bonus: 0, cap:100}
];


function calcBroadwayTest() {
    for (var atar = 75; atar < 100; atar += 0.1) {
        console.log("calcBroadwayTest: " + atar + " -> " + calcBroadway(atar, BROADWAY_RULES) );
    }
}

function calcBroadway(atar, BROADWAY_RULES) {
    'use strict';
    var rules = BROADWAY_RULES.find(function(a) { return atar < a.max });
    var bonus_atar = atar + rules.bonus;
    if (bonus_atar > rules.cap) {
        bonus_atar = rules.cap;
    }
    return bonus_atar;
}
