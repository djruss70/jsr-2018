// document.addEventListener('deviceready', onDeviceReady, false);

// function onDeviceReady()
// {


var coursesSetup = {
	MAA : {
		name : "Applications",
		m : 0.9,
		b : 0.0,
		exclusive : ['MAM', 'MAS'],
	},
	MAM : {
		name : "Methods",
		m : 1.1,
		b : 0.0,
		exclusive : ['MAA'],
	},
	MAS : {
		name : "Specialist",
		m : 1.2,
		b : 0.0,
		exclusive : ['MAA']
	},
	ITA : {
		name : "Italian",
		m : 1.0,
		b : 0.0,
	},
	ENG : {
		name : "English",
		m : 1.0,
		b : 0.0,
	},
	LIT : {
		name : "Literature",
		m : 1.1,
		b : 0.0,
	},
	PHY : {
		name : "Physics",
		m : 1.2,
		b : 0.0,
	},
	CHE : {
		name : "Chemistry",
		m : 1.2,
		b : 0.0,
	},
	HBE : {
		name : "Human Biology",
		m : 0.9,
		b : 0.0,
	},
	REL : {
		name : "Religion",
		m : 0.8,
		b : 0.0,
	},
}


function buildUI() {

	// build user course selection rows

	var coursesCount = 6;
	var htmlStr = '';

	for (var i = 0; i < coursesCount; i++) {

		// course settings table
		htmlStr += `
		<div class="row">
          <div class="col-xs-5 mt-0">
            <select class="custom-select d-block w-100" id="course${i}" required>
            </select>
          </div>
          <div class="col-xs-5 mt-5">
              <div class="slidecontainer">
                  <input type="range" min="1" max="100" value="50" class="slider" id="slider${i}">
              </div>
          </div>
          <div class="col-xs-2 mt-3">
                <p id="rawScore${i}">0</p>
                <p id="scaledScore${i}">0</p>
          </div>
        </div>`;

	}
	document.getElementById(`courses`).innerHTML = htmlStr;



	for (var i = 0; i < coursesCount; i++) {
		var htmlStr = `<option value="">Choose course...</option>`;
		for (var j in coursesSetup) {
			htmlStr += `<option>${coursesSetup[j].name}</option>`;
		}
    	document.getElementById(`course${i}`).innerHTML = htmlStr;
    }


  	var atarScore = 0;

  	var coursesList = [];


	for (var i = 0; i < coursesCount; i++) {

		coursesList[i] = {
			rawScore : 0,
			scaledScore : 0
		}


		// document.getElementById(`slider${i}`).oninput = function() { 
		// 	sliderInput(i, this.value, 
		// 		document.getElementById(`rawScore${i}`), 
		// 		document.getElementById(`scaledScore${i}`) 
		// 	) }

	}



		document.getElementById(`course0`).oninput = function() { 
			var keys = Object.keys(coursesSetup);
			var key = keys.find( k => coursesSetup[k].name === this.value );
			coursesList[0].course = key;
			console.log(key, this.value);
		}
		document.getElementById(`course1`).oninput = function() { 
			var keys = Object.keys(coursesSetup);
			var key = keys.find( k => coursesSetup[k].name === this.value );
			coursesList[1].course = key;
			console.log(key, this.value);
		}
		document.getElementById(`course2`).oninput = function() { 
			var keys = Object.keys(coursesSetup);
			var key = keys.find( k => coursesSetup[k].name === this.value );
			coursesList[2].course = key;
			console.log(key, this.value);
		}
		document.getElementById(`course3`).oninput = function() { 
			var keys = Object.keys(coursesSetup);
			var key = keys.find( k => coursesSetup[k].name === this.value );
			coursesList[3].course = key;
			console.log(key, this.value);
		}
		document.getElementById(`course4`).oninput = function() { 
			var keys = Object.keys(coursesSetup);
			var key = keys.find( k => coursesSetup[k].name === this.value );
			coursesList[4].course = key;
			console.log(key, this.value);
		}
		document.getElementById(`course5`).oninput = function() { 
			var keys = Object.keys(coursesSetup);
			var key = keys.find( k => coursesSetup[k].name === this.value );
			coursesList[5].course = key;
			console.log(key, this.value);
		}


		document.getElementById(`slider0`).oninput = function() { 
			sliderInput(0, this.value, 
				document.getElementById(`rawScore0`), 
				document.getElementById(`scaledScore0`) 
			) }
		document.getElementById(`slider1`).oninput = function() { 
			sliderInput(1, this.value, 
				document.getElementById(`rawScore1`), 
				document.getElementById(`scaledScore1`) 
			) }
		document.getElementById(`slider2`).oninput = function() { 
			sliderInput(2, this.value, 
				document.getElementById(`rawScore2`), 
				document.getElementById(`scaledScore2`) 
			) }
		document.getElementById(`slider3`).oninput = function() { 
			sliderInput(3, this.value, 
				document.getElementById(`rawScore3`), 
				document.getElementById(`scaledScore3`) 
			) }
		document.getElementById(`slider4`).oninput = function() { 
			sliderInput(4, this.value, 
				document.getElementById(`rawScore4`), 
				document.getElementById(`scaledScore4`) 
			) }
		document.getElementById(`slider5`).oninput = function() { 
			sliderInput(5, this.value, 
				document.getElementById(`rawScore5`), 
				document.getElementById(`scaledScore5`) 
			) }

	function sliderInput (n, rawScore, rawScoreEl, scaledScoreEl) {
			console.log(`slider ${n}`);	
			// var rawScore = el.value;
			coursesList[n] = coursesList[n] || {};
			coursesList[n].rawScore = rawScore;
			coursesList[n].scaledScore = Math.round(1.1 * rawScore);
			atarScore = calcAtar(coursesList.map(a => a.scaledScore));
			rawScoreEl.innerHTML = coursesList[n].rawScore;
			scaledScoreEl.innerHTML = coursesList[n].scaledScore;
	    	atar.innerHTML = atarScore;
	}





}

function calcAtar(scaledScores) {
	// placeholder function

	// var scaledScore = calcAtar(coursesList.map(a => a.scaledScore));

	//console.log(scaledScores, scaledScores.count);

	var atarScore =  scaledScores.reduce( (av, a) => av + a / scaledScores.length);
	return Math.round(-1 * atarScore);
}
