
// Utility Functions


function round(x, d) {
    if (typeof x !== 'number') {
        return '';
    }
    // 0 dp, if not specified
    var d = d || 0;
    return Math.round(x * Math.pow(10, d)) / Math.pow(10, d);
}


function weightedMean (a, b, aWeight) {
    var bWeight = 1 - aWeight;
    return a * aWeight + b * bWeight;
}


//  Bivariate Data Functions


function linInterp (x, lo, hi) {
    var m = (hi.y - lo.y) / (hi.x - lo.x);
    var y = lo.y + m * (x - lo.x);
    return y;
}

function linScale (x, coeff) {
    return coeff.m * x + coeff.b;
}

function linRegress(xIn, yIn) {
    var sum_x = 0;
    var sum_y = 0;
    var sum_xy = 0;
    var sum_xx = 0;
    var sum_yy = 0;
    var count = 0;

    var values_length = Math.max(xIn.length, yIn.length);
    var values_x = [];
    var values_y = [];

    var lr = {};


    for (var v = 0; v < values_length; v++) {
        if ((values_x[v] != NO_DATA) && (values_y[v] != NO_DATA)) {
            values_x.push(xIn[v]);
            values_y.push(yIn[v]);
        }
    }

    //console.log('linRegress Val', values_x, values_y);


    // We'll use those variables for faster read/write access.
    var x = 0;
    var y = 0;
    var values_length = values_x.length;
    var chartData = [];

    if (values_length != values_y.length) {
        throw new Error('The parameters values_x and values_y need to have same size!');
    }

    // Nothing to do.
    if (values_length == 0) {
        return {m: 1, b: 0};
    }

    if (values_length == 1) {
        return {m: values_y[0]/values_x[0], b:0};
    }

    // Calculate the sum for each of the parts necessary.
    for (var v = 0; v < values_length; v++) {
        x = values_x[v];
        y = values_y[v];
        sum_x += x;
        sum_y += y;
        sum_xx += x*x;
        sum_xy += x*y;
        sum_yy += y*y;
        count++;
        chartData.push({x: x, y: y});
    }
    //addScatterChart({title: "linRegress " + count}, chartData);


    // Calculate m and b for the formula:
    // y = x * m + b
    lr.m = (count*sum_xy - sum_x*sum_y) / (count*sum_xx - sum_x*sum_x);
    lr.b = (sum_y/count) - (lr.m*sum_x)/count;

    // Calculate r2
    lr.r = (count*sum_xy - sum_x*sum_y)/Math.sqrt((count*sum_xx-sum_x*sum_x)*(count*sum_yy-sum_y*sum_y));
    lr.r2 = Math.pow(lr.r, 2);
    lr.n = count;

    // Function to evaluate
    lr.fn = function (x) { return this.m * x + this.b; }

    return lr;
}



function meanSquareError (points) {
    var n = points.length;
    var mse = points.reduce(function(sum, a){ return sum + a.y * a.y / n}, 0);

    return mse;
}

// Miscellaneous Bivariate Data

function percentError (points) {
	return points.map(function(a){ return {x: a.x, y: 100 * (a.x - a.y) / a.x} });
}
